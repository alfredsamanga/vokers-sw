package com.econetwireless.epay.business.integrations.impl;

import com.econetwireless.epay.business.integrations.api.ChargingPlatform;
import com.econetwireless.epay.business.services.impl.EnquiriesServiceImpl;
import com.econetwireless.epay.business.utils.MessageConverters;
import com.econetwireless.in.webservice.BalanceResponse;
import com.econetwireless.in.webservice.CreditRequest;
import com.econetwireless.in.webservice.IntelligentNetworkService;
import com.econetwireless.utils.pojo.INBalanceResponse;
import com.econetwireless.utils.pojo.INCreditRequest;
import com.econetwireless.utils.pojo.INCreditResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by tnyamakura on 17/3/2017.
 */
public class ChargingPlatformImpl implements ChargingPlatform{


    private static final Logger LOGGER = LoggerFactory.getLogger(ChargingPlatformImpl.class);
    private IntelligentNetworkService intelligentNetworkService;

    public ChargingPlatformImpl(IntelligentNetworkService intelligentNetworkService) {
        this.intelligentNetworkService = intelligentNetworkService;
    }

    @Override
    public INBalanceResponse enquireBalance(final String partnerCode, final String msisdn) {
        LOGGER.info("ChargingPlatformImpl enquireBalance. Partner code is: {} and Msisdn is: {}", partnerCode, msisdn);
        final BalanceResponse balanceResponse = intelligentNetworkService.enquireBalance("hot-recharge", msisdn);
        LOGGER.info("\n\nBalance Response is: \nAMOUNT: {}\nMSISDN: {}\nNARRATIVE: {}\nRESPONSE CODE: {}\n", balanceResponse.getAmount(),
                balanceResponse.getMsisdn(), balanceResponse.getNarrative(), balanceResponse.getResponseCode());
        return MessageConverters.convert(balanceResponse);
    }

    @Override
    public INCreditResponse creditSubscriberAccount(final INCreditRequest inCreditRequest) {
        final CreditRequest creditRequest = MessageConverters.convert(inCreditRequest);
        return MessageConverters.convert(intelligentNetworkService.creditSubscriberAccount(creditRequest));
    }
}
